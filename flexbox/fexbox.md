Flex-box c'est coder en CSS natif sans utiliser bootstrap

1) creer un style `container ` qu' on appel box 

exemple : `

- display: flex pour les mettre en ligne horisontal

- justify-content : space-between : pour mettre de l'espace entre les box

https://css-tricks.com/snippets/css/a-guide-to-flexbox/
````
.box{
    height: 300px;
    width: 500px;
    border: black thin solid;
    background: green;
}

.container-main{
    justify-content: space-between;
    display: flex;
}```