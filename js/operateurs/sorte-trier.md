## Sort

`sort` permet de trier Array avec la `la compareFunction`

Exemple: 

````js
const sortdArray = tab.sort( comparefn:(user1, user2)) => user2.age -user1.age)

console.table(sortedArray)
```


```js 
const hotels = [ { name: "Ibis", size: 3}, { name: "Formule 1", size: 5}, { name: "Accor", size: 3} ]
hotels.sort((h1, h2) => h1.size - h2.size)
````