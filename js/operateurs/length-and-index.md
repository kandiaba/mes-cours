## length 

`tab.length` donne la longueur d'un tableau

Exemple : 
```
tab [ tota, toto, titi, tutu ]
`console.log(tab.length)` affiche 4 dans notre cas

- 'tab[0]' donne le premier element

- `tab[tab.length - 1])` donne le dernier element d'un tableau`
``