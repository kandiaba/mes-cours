## Tableau : Array

- ``array``  est une list d'objets ou d'element designer par ``[]`

- ` index` donne la position d'un element dans un tableau et commence a compter a partir de 0

Exemple

 ````
 console.log(tab[1]) affiche l'element 1 dans la console

- tab(X) si `X n'existe pas le resultat affiche undifined
```