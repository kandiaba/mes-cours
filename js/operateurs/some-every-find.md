## Find

``find()` renvoie la valeur du premier élément trouvé dans le tableau qui respecte la condition donnée par la fonction  passée en argument.

Exemple

```js
const persons = [ { "name": "Karim", age: 15 }, { "name": "Jean", age: 17 }, { "name": "Jacques", age: 20 } ]

const isMajor = persons.find(person =>person.age > 18)

Resultat 

//Object { name: "Jacques", age: 20 }
```

Exemple en fonction 

```js
const persons = [ { "name": "Karim", age: 15 }, { "name": "Jean", age: 17 }, { "name": "Jacques", age: 20 } ]

function isMajor(person) {
 return person.age >= 18
}

console.log( persons.find(isMinor) )
//Object { name: "Jacques", age: 20 }
```

## Some

``Some()`` teste si au moins un élément du tableau passe le test implémenté par la fonction fournie. Elle renvoie un booléen indiquant le résultat du test.

Exemple 

```js
const persons = [ { "name": "Jean", age: 17 }, { "name": "Jacques", age: 20 } ]

const isMajor = persons.some(person =>person.age >18)

console.log( isMajor)
//true
```

Exemple en fonction 
``
```js
const persons = [ { "name": "Jean", age: 17 }, { "name": "Jacques", age: 20 } ]

function isMajor(person) {
  return person.age >= 18
}
console.log( persons.some(isMajor) )
// true
```

## every 
``every()` permet de tester si tous les éléments d'un tableau vérifient une condition donnée par une fonction en argument. Cette méthode renvoie un booléen pour le résultat du test.

Exemple

```js
const persons = [ { "name": "Jean", age: 17 }, { "name": "Jacques", age: 2 } ];
const isMajor = persons.every(person =>person.age > 18)
// true 
````