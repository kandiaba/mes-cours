## Map 

`map` permet d'obtenir un nouveau tableau à partir des valeurs dans le tableau initial.
Attention il faut retourner une valeur dans la fonction passée à `map`.

## Exemple: 

const tab2= tab.map(user:(...)..., index:number) => {
return index; })

console.table(tab);

console.table(tab2);

```js
const names = ["Jean", "Toto", "Jean", "Jojo", "Emmanuelle"]
names.map(n => n.toUpperCase())
//Renvoie ["JEAN", "TOTO", "JEAN", "JOJO", "EMMANUELLE"]
``` 

```js
[1, 2, 3, 4, 5].map(function(x){ return x*2 })
[1, 2, 3, 4, 5].map(x =>  x*2)
[1, 2, 3, 4, 5].map(x => { return x*2 } )
```