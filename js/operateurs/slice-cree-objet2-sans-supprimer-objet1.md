.## Slice

``Slice`` Cree un nouveau tableau sans modifié le tableau (ou l'objet) . 

Exemple :coupe a gauche et à droite des elements indiqués

````js
- consttab2 = [1, 2, 3, 4, 5, 6];

consttab3 = tab2.slice(1, 4);

console.table(tab3)

renvoie 

[2, 3, 4] 