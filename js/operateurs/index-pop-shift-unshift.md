## Notre  tableau 

```js 
var fruits = ['Apple', 'Banana', Orange]

```

## Accéder (via son index) à un élément du tableau

```js
var first = fruits [0];

// Apple

var last = fruits[fruits.length - 1];
// Banana
```

## Pop Supprime le dernier élément du tableau

```js
last = fruits.pop(); supprime orange (à la fin)
["Apple", "Banana"];
```

## shift supprime le premier élément du tableau

```js
var first = fruits.shift(); supprime Apple
[Banana, orange];
```

