## Foreach

``foreach`` parcour les éléments du tableau et  renvoi un element demandé

## Exemple: 

- tab.foreach(user:()!..., index:number) =>{'l'user dont l'index est ${index} s'appelle ${user.firtName}'});

- tab.foreach( user=> console.log(user.age + 10))  affiche l'age des gens +10

```js
const names = ["Jean", "Toto", "Jean", "Jojo", "Emmanuelle"]
names.forEach((name, index) => console.log(name, index))
``` 
