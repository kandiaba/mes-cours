## Splice

Permet d'inserer des elements dans array ou d'en supprimer

Exemple: 


- Pour inserer `6`

````js
const tab2[1,2,3,4,5,7]

tab2.splice(start:5,delecount:0,item:6)

console.log(tab2)
````


- Pour supprimer un element
`
```js
tab2.splice(star5, deletecount: 1);
```

Exemple 
````js
const months = ['Jan', 'March', 'April', 'June'];
months.splice(1, 1, 'Feb'); // supprime "March" et remplace par 'Feb'
Object.values(hostels).sort((h1, h2) => h2.roomNumbers - h1.roomNumbers).map(h => h.name)
````