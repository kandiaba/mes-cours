# Bootstrap
#### Bootstrap est une collection d'outils utiles à la création du design de sites et d'applications web. C'est un ensemble qui contient des codes HTML et CSS, des formulaires, boutons, outils de navigation et autres éléments interactifs, ainsi que des extensions JavaScript en option
## Instalation NPM
- `npm init` :  permet d'initialiser un projet NPM
- `npm install nom-package` : permet d'ajouter un package à mon projet en mettant à jour le 
fichier `package.json` et il télécharge en même temps les packages dans le dossier `node_modules`. Les packages installés avec npm sont aussi appelés les dépendences de mon projet.
-npm install Bootstrap` permet d'installer Bootstrap.

Tous les modules de mon projet peuvent être réinstallés grâce à la commande `npm install`. 

## Link Bootstrap dans mon ``Ìndex.html ``
-  "<link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.css">"

##Utilisation `bootstrap`
on commence par la classe ``<div class="container">``
contenu 
