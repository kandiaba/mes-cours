## Row et Col
- Row : ligne
- col : Colonne (12 colonnes en somme)
- container : Contenu fix  
- container-fluid 

Row et col sont toujours en couple 
on peut mettre plusieur couple de de ``row et col dans une col``
mais un element seul doit etre toujour dans une col et un Row

- Les `ofset` permettent de decaler les colonnes restantes .  
## exemple

    <div class="row content">
        <div class="col-4">
            <div class="row ">
                <div class="col">
                    <div class="row photo">
                        <div class="col">
                        <img src="../assets/images/portrait.jpeg">
                        </div>
                    </div>
                    <div class="row presentation">
                        <div class="col">Presentation</div>
                    </div>
                </div>
            </div>

            <div class="row qualite">
                <div class="col">

                </div>
            </div>
        </div>